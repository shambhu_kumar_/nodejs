var express = require('express');
var nodemailer = require("nodemailer");
var router = express.Router();
/* GET home page. */
router.get('/', ensureAuthenticated, function (req, res, next) {
    res.render('contact', {
        title: 'Contact'
    });
});

function ensureAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/users/login');
}
router.post('/send', ensureAuthenticated, function (req, res, next) {
    var transporter = nodemailer.createTransport({
        service: 'Gmail'
        , auth: {
            user: 'love.sam@gmail.com'
            , pass: 'gayadham'
        }
    });
    var mailOptions = {
        from: 'Shambhu Kumar'
        , to: 'ss2k1in@gmail.com'
        , subject: 'Website Submission'
        , text: 'You have a submission with the following details... Name: ' + req.body.name + 'Email: ' + req.body.email + ' Message: ' + req.body.message
        , html: '<p>You have a submission with the following details... Name: </p><ul><li>Name: ' + req.body.name + '</li><li>Email: ' + req.body.email + '</li><li>Message: ' + req.body.message + '</li></ul>'
    };
    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
            res.redirect('/');
        }
        else {
            console.log('Message Sent: ' + info.response);
            res.redirect('/');
        }
    });
});
module.exports = router;