var express = require('express');
var router = express.Router();
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var userservice = require('../service/userservice');
var bcrypt = require('bcryptjs');
//
//
passport.serializeUser(function (user, done) {
    done(null, user.id);
});
// used to deserialize the user
passport.deserializeUser(function (id, done) {
    var data = {
        id: id
    };
    userservice.userdetails(data, function (err, rows) {
        done(err, rows[0]);
    });
});
passport.use('local-login', new LocalStrategy({
    passReqToCallback: true // allows us to pass back the entire request to the callback
}, function (req, username, password, done) { // callback with email and password from our form
    var data = {
        username: username
    };
    userservice.userdetails(data, function (err, rows) {
        if (err) return done(err);
        if (!rows.length) {
            return done(null, false, req.flash('error', 'No user found.'));
        }
        userservice.comparePassword(password, rows[0].password, function (err, isMatch) {
            console.log("passport.use comparePassword");
            if (err) return done(err);
            if (isMatch) {
                return done(null, rows[0]);
            }
            else {
                return done(null, false, {
                    message: 'Invalid Password'
                });
            }
        });
    });
}));
//
passport.use('local-signup', new LocalStrategy({
    passReqToCallback: true // allows us to pass back the entire request to the callback
}, function (req, username, password, done) {
    console.log("req: ", req);
    console.log("req body: ", req.body);
    var data = {
        username: username
    };
    userservice.userdetails(data, function (err, rows) {
        if (err) return done(err);
        if (rows.length) {
            return done(null, false, req.flash('error', 'That username is already taken.'));
        }
        else {
            console.log("req.body: ", req.body)
            userservice.create(req.body, function (err, user) {
                if (err) {
                    return done(null, false, req.flash('error', 'There is problem in processing.'));
                }
                return done(null, user);
            });
        }
    });
}));
//
/* GET users listing. */
router.get('/login', function (req, res, next) {
    res.render('login', {
        title: 'Login'
        , errors: null
        , username: ''
        , password: ''
    });
});
router.post('/login', function (req, res) {
    var username = req.body.username;
    var password = req.body.password;
    req.checkBody('username', 'username should not be empty').notEmpty();
    req.checkBody('password', 'password should not be empty').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        res.render('login', {
            title: 'Login'
            , errors: errors
            , username: username
            , password: password
        });
    }
    else {
        passport.authenticate('local-login', {
            successRedirect: '/', // redirect to the secure profile section
            failureRedirect: '/users/login', // redirect back to the signup page if there is an error
            failureFlash: 'Invalid username or password'
        })(req, res);
    }
});
router.get('/register', function (req, res, next) {
    console.log('respond with a Register get');
    res.render('register', {
        title: 'Registration'
        , errors: null
    });
});
router.post('/register', function (req, res) {
    console.log('respond with a Register post');
    var username = req.body.username;
    var password = req.body.password;
    var password2 = req.body.password2;
    var email = req.body.email;
    var name = req.body.name;
    req.checkBody('username', 'username should not be empty').notEmpty();
    req.checkBody('password', 'password should not be empty').notEmpty();
    req.checkBody('password2', 'confirm password should not be empty').notEmpty();
    req.checkBody('password2', 'confirm password not matching to password').equals(password);
    req.checkBody('email', 'email should not be empty').notEmpty();
    req.checkBody('email', 'email is not valid').isEmail();
    req.checkBody('name', 'name should not be empty').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        res.render('register', {
            title: "Registration"
            , user: null
            , errors: errors
        });
    }
    else {
        passport.authenticate('local-signup', {
            successRedirect: '/', // redirect to the secure profile section
            failureRedirect: '/users/register', // redirect back to the signup page if there is an error
            failureFlash: true // allow flash messages
        })(req, res);
    }
});
router.get('/logout', function (req, res) {
    req.logout();
    req.flash('success', 'You are now logged out');
    res.redirect('/users/login');
});
module.exports = router;