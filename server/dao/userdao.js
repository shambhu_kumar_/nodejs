var db = require('../utils/database.js');
var model = require('../model/user.js');
exports.userdetails = function (data, callback) {
    var sql = model.query;
    if ((data.username != "" && data.username != undefined) && data.username.length > 0) {
        sql += "and username='" + data.username + "'";
    }
    /*if ((data.password != "" && data.password != undefined) && data.password.length > 0) {
    sql += "and password='" + data.password + "'";
}*/
    if ((data.id != "" && data.id != undefined) && data.id > 0) {
        sql += " and id=" + data.id;
    }
    console.log("SQL : " + sql);
    // get a connection from the pool
    db.mysqlPool.getConnection(function (err, connection) {
        if (err) {
            callback(true);
            connection.release();
            return;
        }
        // make the query
        connection.query(sql, function (err, results) {
            if (err) {
                callback(true);
                return;
            }
            console.log("result on select", results);
            callback(false, results);
            connection.release();
        });
    });
};
exports.create = function (data, callback) {
    var sql = model.insert;
    var user = {
        username: data.username
        , password: data.password
        , name: data.name
        , email: data.email
    };
    console.log("in dao: ", user);
    // get a connection from the pool
    db.mysqlPool.getConnection(function (err, connection) {
        if (err) {
            callback(true);
            connection.release();
            return;
        }
        // make the query
        connection.query(sql, user, function (err, rows) {
            if (err) {
                callback(true);
                return;
            }
            // callback(false);
            user.id = rows.insertId;
            callback(false, user);
            connection.release();
        });
    });
};
exports.delete = function (data, callback) {
    var sql = model.delete + "(" + data.toString() + ")";
    console.log(sql);
    // get a connection from the pool
    db.mysqlPool.getConnection(function (err, connection) {
        if (err) {
            callback(true);
            connection.release();
            return;
        }
        // make the query
        connection.query(sql, function (err) {
            if (err) {
                callback(true);
                return;
            }
            callback(false);
            connection.release();
        });
    });
};
exports.update = function (data, callback) {
    var sql = model.update;
    // get a connection from the pool
    db.mysqlPool.getConnection(function (err, connection) {
        if (err) {
            callback(true);
            connection.release();
            return;
        }
        // make the query
        connection.query(sql, [data.username, data.password, data.name, data.email, data.id], function (err) {
            if (err) {
                callback(true);
                return;
            }
            callback(false);
            connection.release();
        });
    });
};