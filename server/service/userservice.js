var bcrypt = require('bcryptjs');
var userdao = require('../dao/userdao');
exports.userdetails = function (data, callback) {
    userdao.userdetails(data, function (err, results) {
        if (err) {
            callback(true);
            return;
        }
        callback(false, results);
    });
};
exports.create = function (data, callback) {
    bcrypt.genSalt(10, function (err, salt) {
        bcrypt.hash(data.password, salt, function (err, hash) {
            data.password = hash;
            console.log(data.password);
            if (data.id > 0) {
                userdao.update(data, function (err) {
                    if (err) {
                        callback(true);
                        return;
                    }
                    callback(false);
                });
            }
            else {
                userdao.create(data, function (err, user) {
                    if (err) {
                        callback(true);
                        return;
                    }
                    callback(false, user);
                });
            }
        });
    });
};
exports.delete = function (data, callback) {
    userdao.delete(data, function (err) {
        if (err) {
            callback(true);
            return;
        }
        callback(false);
    });
};
exports.comparePassword = function (candidatePassword, hash, callback) {
    bcrypt.compare(candidatePassword, hash, function (err, isMatch) {
        callback(null, isMatch);
    });
}